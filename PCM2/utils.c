#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#pragma warning (disable:4996)
void save_layer(HeatBuffer* buff) {
    FILE* f;
    int i, j;
    char npy[] = "_n.npy";
    char res_name[FILE_NAME_SIZE] = { 0 };
    double* row = malloc(sizeof(double) * buff->N);
    MPI_Gather(buff->v_prev, buff->per_proc * buff->N, MPI_DOUBLE, buff->v, buff->per_proc * buff->N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (buff->rank == 0) {
        itoa(buff->N, res_name, 10);
        strcat(res_name, npy);
        f = fopen(res_name, "wb");
        for (i = 0; i < buff->N; i++) {
            for (j = 0; j < buff->N; j++)
                row[j] = buff->v[j * buff->N + i];
            //fwrite(buff->A + i * buff->n_cols, sizeof(double), buff->n_cols, f);
            fwrite(row, sizeof(double), buff->N, f);
        }
        fclose(f);
        free(row);
    }
}