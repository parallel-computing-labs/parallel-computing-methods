
#include "mpi.h"
#include <stdlib.h>
#include <math.h>
#include "linalg.h"


void time_step(HeatBuffer* buff) {
    int i, j, k;
    if (buff->rank != 0) {
        for (i = 1; i < buff->N - 1; i++) {
            k = i;
            buff->v_cur[k] = (1 - buff->ap) * buff->v_prev[k] + buff->ae * buff->v_prev[k + 1] + buff->aw * buff->v_prev[k - 1] + buff->an * buff->v_prev[k + buff->N] + buff->as * buff->fict_elements[i];
        }
    }
    if (buff->rank != buff->n_proc - 1) {
        for (i = 1; i < buff->N - 1; i++) {
            k = (buff->per_proc - 1) * buff->N + i;
            buff->v_cur[k] = (1 - buff->ap) * buff->v_prev[k] + buff->ae * buff->v_prev[k + 1] + buff->aw * buff->v_prev[k - 1] + buff->an * buff->fict_elements[buff->N + i] + buff->as * buff->v_prev[k - buff->N];
        }
    }
    for (i = 1; i < buff->N - 1; i++)
        for (j = 1; j < buff->per_proc - 1; j++) {
            k = i + j * buff->N;
            buff->v_cur[k] = (1 - buff->ap) * buff->v_prev[k] + buff->ae * buff->v_prev[k + 1] + buff->aw * buff->v_prev[k - 1] + buff->an * buff->v_prev[k + buff->N] + buff->as * buff->v_prev[k - buff->N];
        }
}


void set_init(HeatBuffer* buff) {
    int i, j;
    if (buff->rank == 0) {
        buff->v = malloc(sizeof(double) * buff->N * buff->N);
        for (i = 0; i < buff->N; i++)
            for (j = 0; j < buff->N; j++) {
                buff->v[i * buff->N + j] = buff->u0(i * buff->h, j * buff->h);
            }

    }
    buff->v_prev = malloc(sizeof(double) * buff->per_proc * buff->N);
    buff->v_cur = malloc(sizeof(double) * buff->per_proc * buff->N);
    MPI_Scatter(buff->v, buff->per_proc * buff->N, MPI_DOUBLE, buff->v_cur, buff->per_proc * buff->N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (buff->rank == 0) {
        for (i = 1; i < buff->N - 1; i++) {
            buff->v_prev[i] = buff->v_cur[i] = buff->uB;
        }
    }
    if (buff->rank == buff->n_proc - 1) {
        for (i = 1; i < buff->N - 1; i++) {
            buff->v_prev[(buff->per_proc - 1) * buff->N + i] = buff->v_cur[(buff->per_proc - 1) * buff->N + i] = buff->uB;
        }
    }
    for (j = 0; j < buff->per_proc; j++) {
        buff->v_prev[j * buff->N] = buff->v_prev[j * buff->N + buff->N - 1] = buff->uB;
        buff->v_cur[j * buff->N] = buff->v_cur[j * buff->N + buff->N - 1] = buff->uB;
    }
}
void constructFictElementdHeat(HeatBuffer* buff) {
    buff->fict_elements = calloc(2 * buff->N, sizeof(double));
    buff->scs = calloc(buff->n_proc, sizeof(int));
    buff->sds = calloc(buff->n_proc, sizeof(int));
    buff->rcs = calloc(buff->n_proc, sizeof(int));
    buff->rds = calloc(buff->n_proc, sizeof(int));
    if (buff->rank != 0) {
        buff->scs[buff->rank - 1] = buff->N;
        buff->rcs[buff->rank - 1] = buff->N;
        buff->sds[buff->rank - 1] = 0;
        buff->rds[buff->rank - 1] = 0;
    }
    if (buff->rank != buff->n_proc - 1) {
        buff->scs[buff->rank + 1] = buff->N;
        buff->rcs[buff->rank + 1] = buff->N;
        buff->sds[buff->rank + 1] = (buff->per_proc - 1) * buff->N;
        buff->rds[buff->rank + 1] = buff->N;
    }
    MPI_Alltoallv(buff->v_cur, buff->scs, buff->sds, MPI_DOUBLE, buff->fict_elements, buff->rcs, buff->rds, MPI_DOUBLE, MPI_COMM_WORLD);

}


void HeatDiff(HeatBuffer* hbuff) {
    int step;
    double* tmp;
    for (step = 0; step < hbuff->time_steps; step++) {
        tmp = hbuff->v_prev;
        hbuff->v_prev = hbuff->v_cur;
        hbuff->v_cur = tmp;
        time_step(hbuff);
        MPI_Alltoallv(hbuff->v_cur, hbuff->scs, hbuff->sds, MPI_DOUBLE, hbuff->fict_elements, hbuff->rcs, hbuff->rds, MPI_DOUBLE, MPI_COMM_WORLD);
    }
}