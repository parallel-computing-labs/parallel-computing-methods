#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include <math.h>
#include "linalg.h"
#pragma warning (disable:4996)
#define DIMENSIONS_NUMBER 1
static int problem_dim[] = { 240 };
static double T = 0.1;
static double L = 2;
static double uB = 0;
#define REPEATS 50
#define SAVE_FINAL_TIME_LAYER 1

double u0(double x, double y) {
    return exp(-8 * (x - 1) * (x - 1) - 8 * (y - 1) * (y - 1));
}
int main(int argc, char** argv) {
    int world_rank, world_size;
    int i, j;
    char name[FILE_NAME_SIZE] = { 0 };
    char txt[] = "_mpi_procs.txt";
    double start, end, time, time_squared;
    FILE* f = NULL;
    HeatBuffer* hbuff;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_rank == 0) {
        itoa(world_size, name, 10);
        strcat(name, txt);
        f = fopen(name, "w");
    }
    for (i = 0; i < DIMENSIONS_NUMBER; i++) {
        time = time_squared = 0;
        hbuff = malloc(sizeof(HeatBuffer));
        hbuff->rank = world_rank;
        hbuff->n_proc = world_size;
        hbuff->N = problem_dim[i];
        hbuff->L = L;
        hbuff->h = L / (hbuff->N - 1);
        hbuff->tau = hbuff->h * hbuff->h / 8;
        hbuff->time_steps = (int)(8. / L / L * T * (problem_dim[0] - 1) * (problem_dim[0] - 1));
        hbuff->u0 = u0;
        hbuff->per_proc = hbuff->N / hbuff->n_proc;
        hbuff->ae = hbuff->aw = hbuff->tau / hbuff->h / hbuff->h;
        hbuff->an = hbuff->as = hbuff->tau / hbuff->h / hbuff->h;
        hbuff->ap = hbuff->ae + hbuff->aw + hbuff->an + hbuff->as;
        hbuff->uB = uB;
        for (j = 0; j < REPEATS; j++) {
            set_init(hbuff);
            constructFictElementdHeat(hbuff);
            MPI_Barrier(MPI_COMM_WORLD);
            start = MPI_Wtime();

            HeatDiff(hbuff);

            MPI_Barrier(MPI_COMM_WORLD);
            end = MPI_Wtime();
            time += end - start;
            time_squared += (end - start) * (end - start);
            if ((j == REPEATS - 1) && SAVE_FINAL_TIME_LAYER)
                save_layer(hbuff);
            if (world_rank == 0)
                free(hbuff->v);
            free(hbuff->fict_elements);
            free(hbuff->scs);
            free(hbuff->sds);
            free(hbuff->rcs);
            free(hbuff->rds);
            free(hbuff->v_prev);
            free(hbuff->v_cur);
        }
        free(hbuff);
        time /= REPEATS;
        time_squared /= REPEATS;
        if (world_rank == 0) {

            fprintf(f, "%i, %g, %g\n", problem_dim[i], time, sqrt(time_squared - time * time));
        }
    }
    if (world_rank == 0)
    {
        printf("proc = %i, total_procs = %i\n", world_rank, world_size);
        fclose(f);
    }
    MPI_Finalize();
    return 0;
}