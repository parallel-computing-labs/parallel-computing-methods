#pragma once
typedef double (*real2Dfunc)(double x, double y);
typedef struct HeatBuffer {
    double* v;
    double* v_prev, * v_cur;
    int rank, N, n_proc, per_proc;
    double* fict_elements;
    int* scs, * sds, * rcs, * rds;
    real2Dfunc u0;
    double tau;
    int time_steps;
    double h;
    double L;
    double ae, aw, an, as, ap;
    double uB;
} HeatBuffer;
void time_step(HeatBuffer* buff);
void set_init(HeatBuffer* buff);
void constructFictElementdHeat(HeatBuffer* buff);
void HeatDiff(HeatBuffer* hbuff);