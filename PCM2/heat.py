#%%
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
from IPython.display import set_matplotlib_formats, display, Latex
set_matplotlib_formats('svg','pdf')
import pandas as pd
#%%
T = .1   # final time
nx = 30
ny = 30
L_b = 2
#%%
x = np.linspace(0,L_b,nx+2)
y = np.linspace(0,L_b,ny+2)
b = np.zeros((nx+2) * (ny+2), np.float32)
a = 1
h_x = L_b / (nx + 1)
h_y = L_b / (ny + 1)
tau = 0.125 * h_x * h_x
num_steps = int(T / tau)
ae = aw = a * a * tau / h_x / h_x
an = aS = a * a * tau / h_y / h_y
ap = ae + aw + an + aS
a_ = 8
#%%
u_0_func = lambda x,y: np.exp(- a_*np.power(x-1, 2)- a_*np.power(y-1, 2))
v_prev = np.zeros((nx+2) * (ny+2), np.float32)
v_cur = np.zeros((nx+2) * (ny+2), np.float32)
u0 = u_0_func
for i in range(nx + 2):
   for j in range(ny + 2):
      k = (ny + 2) * i + j
      v_prev[k] = u0(x[i], y[j])

xx, yy = np.meshgrid(np.linspace(0,L_b,nx+2),np.linspace(0,L_b,ny+2))
zzz = np.zeros((ny+2,nx+2), np.float32)
for k in range((nx+2) * (ny+2)):
   i,j = k // (ny + 2), k % (ny + 2)
   zzz[j][i] = v_prev[k]

A = np.zeros(((nx+2) * (ny+2),(nx+2) * (ny+2)), np.float32)
LEFT_BOUNDARY = 0
LOWER_BOUNDARY = 0
uB = lambda x,y: 0*x + 0*y
element_count = 0
for j in range(ny + 2):
   #for i = 0
   k = j
   if A[k][k] == 0:
      element_count += 1
   A[k][k] = 1
   b[k] = uB(LEFT_BOUNDARY, y[j])
   #for i = nx + 1
   k = (ny + 2) * (nx + 1) + j
   if A[k][k] == 0:
      element_count += 1
   A[k][k] = 1
   b[k] = uB(L_b, y[j])
   #print(k)

for i in range(nx + 2):
   #for j = 0
   k = (ny + 2) * i 
   if A[k][k] == 0:
      element_count += 1
   A[k][k] = 1
   b[k] = uB(x[i], LOWER_BOUNDARY)
   #for j = ny + 1
   k = (ny + 2) * i + ny + 1
   if A[k][k] == 0:
      element_count += 1
   A[k][k] = 1
   b[k] = uB(x[i], L_b)
   #print(k)

for i in range(1, nx + 1):
   for j in range(1, ny + 1):
      k = i * (ny + 2) + j
      if A[k][k] == 0:
         element_count += 1
      A[k][k] = 1 + ap

      if A[k][k+ny+2] == 0:
         element_count += 1
      A[k][ny + 2 + k] = -ae

      if A[k][k-ny-2] == 0:
         element_count += 1
      A[k][k - ny - 2] = -aw

      if A[k][k + 1] == 0:
         element_count += 1
      A[k][1 + k] = -an

      if A[k][k - 1] == 0:
         element_count += 1
      A[k][k - 1] = -aS
      b[k] = v_prev[k]

#%%
v_cur = np.linalg.solve(A, b)
zzz = np.zeros((ny+2,nx+2), np.float32)
for k in range((nx+2) * (ny+2)):
   i,j = k // (ny + 2), k % (ny + 2)
   zzz[j][i] = v_cur[k]
fig=plt.contourf(xx,yy,zzz, levels=40)
fig.set_cmap('plasma')
fig.set_clim(0,1)
plt.colorbar(fig)
v_prev = v_cur
for l in range(num_steps - 1):
   for i in range(1, nx + 1):
      for j in range(1, ny + 1):
         k = i * (ny + 2) + j
         b[k] = v_prev[k]
   v_cur = np.linalg.solve(A, b)
   if ((l+1) % 20  == 0) | l == num_steps - 2:
      for k in range((nx+2) * (ny+2)):
         i,j = k // (ny + 2), k % (ny + 2)
         zzz[j][i] = v_cur[k]
      fig=plt.contourf(xx,yy,zzz, levels=40)
      fig.set_cmap('plasma')
      fig.set_clim(0,1)
      plt.colorbar(fig)
      plt.show()
   v_prev = v_cur
#%%
ref_N = 32
zzz2=np.fromfile(f'{ref_N}_n.npy').reshape(zzz.shape)
#%%
plt.set_cmap('plasma')
fig, ax = plt.subplots(1,2, figsize=(6,3))
ax[0].contourf(xx,yy,zzz2, levels=30)
ax[0].set_title("explicit scheme")
ax[1].set_title("implicit scheme")
ax[1].contourf(xx,yy,zzz, levels=30)
plt.savefig('solutions_cmp.pdf', bbox_inches='tight')
#%%
fig, ax = plt.subplots(figsize=(3,3))
fig=plt.contourf(xx,yy,np.abs(zzz-zzz2), levels=40)
fig.set_cmap('plasma')
plt.colorbar()
fig.set_clim(0,zzz.max())
plt.savefig('solutions_diff.pdf', bbox_inches='tight')