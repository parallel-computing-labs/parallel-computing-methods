#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#pragma warning(disable:4996)
void pic_read(PMBuffer* buff) {
    FILE* f;
    int i, j;
    double* row;
    if (buff->rank == 0) {
        f = fopen(buff->name, "rb");
        row = malloc(sizeof(double) * buff->n_cols);
        buff->A = malloc(sizeof(double) * buff->n_rows * buff->n_cols);
        for (i = 0; i < buff->n_rows; i++) {
            fread(row, sizeof(double), buff->n_cols, f);
            for (j = 0; j < buff->n_cols; j++)
                buff->A[j * buff->n_rows + i] = row[j];
        }
        fclose(f);
        free(row);
    }
    buff->I_cur = malloc(sizeof(double) * buff->per_proc * buff->n_rows);
    buff->I_prev = malloc(sizeof(double) * buff->per_proc * buff->n_rows);
    MPI_Scatter(buff->A, buff->per_proc * buff->n_rows, MPI_DOUBLE, buff->I_cur, buff->per_proc * buff->n_rows, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    Copy(buff->I_prev, buff->I_cur, buff->per_proc * buff->n_rows);
}

void pic_write(PMBuffer* buff) {
    FILE* f;
    int i, j;
    double* row = malloc(sizeof(double) * buff->n_cols);
    MPI_Gather(buff->I_cur, buff->per_proc * buff->n_rows, MPI_DOUBLE, buff->A, buff->per_proc * buff->n_rows, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (buff->rank == 0) {
        f = fopen(buff->res_name, "wb");
        for (i = 0; i < buff->n_rows; i++) {
            for (j = 0; j < buff->n_cols; j++)
                row[j] = buff->A[j * buff->n_rows + i];
            fwrite(row, sizeof(double), buff->n_cols, f);
        }
        fclose(f);
        free(row);
    }
}