#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "linalg.h"
#include "utils.h"
#pragma warning(disable:4996)


#define REPEATS 1
#define FILE_NAME_SIZE 35
#define SAVE_FINAL_TIME_LAYER 1
#define MAX_THREADS_POW 4
#define N_COLS 960
#define N_ROWS 1138
#define TIME_STEPS 100
#define LAMBDA 0.2
int main(int argc, char** argv) {
    int world_rank, world_size;
    char init_pic_name[FILE_NAME_SIZE] = "init_pic.npy";
    char result_pic_name[FILE_NAME_SIZE] = "res_pic.npy";
    int j;
    char name[FILE_NAME_SIZE] = { 0 };
    char txt[] = "_mpi_procs.txt";
    double start, end, time, time_squared;
    FILE* f = NULL;
    PMBuffer* buff;
    int k, p;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_rank == 0) {
        itoa(world_size, name, 10);
        strcat(name, txt);
        f = fopen(name, "w");
    }
    buff = malloc(sizeof(PMBuffer));
    if (world_rank == 0) {
        buff->name = calloc(FILE_NAME_SIZE, sizeof(char));
        buff->res_name = calloc(FILE_NAME_SIZE, sizeof(char));
        strcat(buff->name, init_pic_name);
        strcat(buff->res_name, result_pic_name);
    }
    buff->n_proc = world_size;
    buff->rank = world_rank;
    buff->A = NULL;
    buff->n_cols = 960;
    buff->n_rows = 1138;
    buff->per_proc = buff->n_cols / buff->n_proc;
    buff->time_steps = 100;
    buff->lambda = 0.2;
    time = time_squared = 0;

    for (j = 0; j < REPEATS; j++) {
        pic_read(buff);
        PMconstructFictElement(buff);
        MPI_Barrier(MPI_COMM_WORLD);
        start = MPI_Wtime();

        PMDiff(buff);

        MPI_Barrier(MPI_COMM_WORLD);
        end = MPI_Wtime();
        time += end - start;
        time_squared += (end - start) * (end - start);
        if ((j == REPEATS - 1) && SAVE_FINAL_TIME_LAYER)
            pic_write(buff);
        if (world_rank == 0) {
            free(buff->A);
        }
        free(buff->fict_elements);
        free(buff->scs);
        free(buff->sds);
        free(buff->rcs);
        free(buff->rds);
        free(buff->I_cur);
        free(buff->I_prev);
    }
    time /= REPEATS;
    time_squared /= REPEATS;
    if (world_rank == 0) {

        fprintf(f, "%i, %g, %g\n", buff->n_cols, time, sqrt(time_squared - time * time));
        printf("proc = %i, total_procs = %i\n", world_rank, world_size);
        free(buff->name);
        free(buff->res_name);
    }
    free(buff);
    
    if (world_rank == 0)
        fclose(f);
    MPI_Finalize();
    return 0;
}