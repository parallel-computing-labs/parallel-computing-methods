#include "linalg.h"
#include "mpi.h"
#include <stdlib.h>
double g(double x) {
    return exp(-x * x / K_PARAMETER / K_PARAMETER);
}

void apply_bc(PMBuffer* buff) {
    int i, j;
    if (buff->rank == 0) {
        for (i = 1; i < buff->n_rows - 1; i++) {
            buff->I_cur[i] = (4 * buff->I_cur[i + buff->n_rows] - buff->I_cur[i + 2 * buff->n_rows]) / 3.;
        }
    }

    if (buff->rank == buff->n_proc - 1) {
        for (i = 1; i < buff->n_rows - 1; i++) {
            buff->I_cur[(buff->per_proc - 1) * buff->n_rows + i] = (4 * buff->I_cur[(buff->per_proc - 2) * buff->n_rows + i] - buff->I_cur[(buff->per_proc - 3) * buff->n_rows + i]) / 3.;
        }
    }
    for (j = 0; j < buff->per_proc; j++) {
        buff->I_cur[j * buff->n_rows] = (4 * buff->I_cur[j * buff->n_rows + 1] - buff->I_cur[j * buff->n_rows + 2]) / 3.;
        buff->I_cur[j * buff->n_rows + buff->n_rows - 1] = (4 * buff->I_cur[j * buff->n_rows + buff->n_rows - 2] - buff->I_cur[j * buff->n_rows + buff->n_rows - 3]) / 3.;
    }
}
void PMtime_step(PMBuffer* buff) {
    int i, j, k;
    double cN, cS, cW, cE;
    double dN, dS, dW, dE;
    if (buff->rank != 0) {
        for (i = 1; i < buff->n_rows - 1; i++) {
            k = i;
            dN = buff->I_prev[k - 1] - buff->I_prev[k];
            dS = buff->I_prev[k + 1] - buff->I_prev[k];
            dE = buff->I_prev[k + buff->n_rows] - buff->I_prev[k];
            dW = buff->fict_elements[i] - buff->I_prev[k];
            cN = g(dN);
            cS = g(dS);
            cE = g(dE);
            cW = g(dW);
            buff->I_cur[k] = buff->I_prev[k] + buff->lambda * (cN * dN + cS * dS * cW * dW + cE * dE);
        }
    }
    if (buff->rank != buff->n_proc - 1) {
        for (i = 1; i < buff->n_rows - 1; i++) {
            k = (buff->per_proc - 1) * buff->n_rows + i;
            dN = buff->I_prev[k - 1] - buff->I_prev[k];
            dS = buff->I_prev[k + 1] - buff->I_prev[k];
            dE = buff->fict_elements[i + buff->n_rows] - buff->I_prev[k];
            dW = buff->I_prev[k - buff->n_rows] - buff->I_prev[k];
            cN = g(dN);
            cS = g(dS);
            cE = g(dE);
            cW = g(dW);
            buff->I_cur[k] = buff->I_prev[k] + buff->lambda * (cN * dN + cS * dS * cW * dW + cE * dE);
        }
    }
    for (k = 1 + buff->n_rows; k <= (buff->n_rows) * (buff->per_proc - 2) + buff->n_rows - 2; k++) {
        dN = buff->I_prev[k - 1] - buff->I_prev[k];
        dS = buff->I_prev[k + 1] - buff->I_prev[k];
        dE = buff->I_prev[k + buff->n_rows] - buff->I_prev[k];
        dW = buff->I_prev[k - buff->n_rows] - buff->I_prev[k];
        cN = g(dN);
        cS = g(dS);
        cE = g(dE);
        cW = g(dW);
        buff->I_cur[k] = buff->I_prev[k] + buff->lambda * (cN * dN + cS * dS * cW * dW + cE * dE);
    }
}

void PMconstructFictElement(PMBuffer* buff) {
    buff->fict_elements = calloc(2 * buff->n_rows, sizeof(double));
    buff->scs = calloc(buff->n_proc, sizeof(int));
    buff->sds = calloc(buff->n_proc, sizeof(int));
    buff->rcs = calloc(buff->n_proc, sizeof(int));
    buff->rds = calloc(buff->n_proc, sizeof(int));
    if (buff->rank != 0) {
        buff->scs[buff->rank - 1] = buff->n_rows;
        buff->rcs[buff->rank - 1] = buff->n_rows;
        buff->sds[buff->rank - 1] = 0;
        buff->rds[buff->rank - 1] = 0;
    }
    if (buff->rank != buff->n_proc - 1) {
        buff->scs[buff->rank + 1] = buff->n_rows;
        buff->rcs[buff->rank + 1] = buff->n_rows;
        buff->sds[buff->rank + 1] = (buff->per_proc - 1) * buff->n_rows;
        buff->rds[buff->rank + 1] = buff->n_rows;
    }
    MPI_Alltoallv(buff->I_cur, buff->scs, buff->sds, MPI_DOUBLE, buff->fict_elements, buff->rcs, buff->rds, MPI_DOUBLE, MPI_COMM_WORLD);
}

void PMDiff(PMBuffer* buff) {
    int step;
    double* tmp;
    for (step = 0; step < buff->time_steps; step++) {
        tmp = buff->I_prev;
        buff->I_prev = buff->I_cur;
        buff->I_cur = tmp;
        PMtime_step(buff);
        apply_bc(buff);
        MPI_Alltoallv(buff->I_cur, buff->scs, buff->sds, MPI_DOUBLE, buff->fict_elements, buff->rcs, buff->rds, MPI_DOUBLE, MPI_COMM_WORLD);
    }
}



void Copy(double* dest, double* src, int n) {
    int i;
    for (i = 0; i < n; i++)
        dest[i] = src[i];
}