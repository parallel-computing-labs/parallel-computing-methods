#%%
import pandas as pd
import numpy as np
import seaborn as sns
sns.set_style('whitegrid')
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('svg','pdf')
import matplotlib.pyplot as plt
#%%
# %%
ps = [1,2,4,6,8,10,12]
stds = {}
means = {}
for p in ps:
    with open(f"{p}_mpi_procs.txt") as f:
        lines = f.readlines()
        for line in lines:
            line_splitted = line[:-1].split(',')
            n = int(line_splitted[0])
            mean = float(line_splitted[1])
            std = float(line_splitted[2])
            if n not in means.keys():
                means[n] = []
                stds[n] = []
            means[n].append(mean)
            stds[n].append(std)
# %%
fig, ax = plt.subplots()
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci =  3*stds_arr
    ax.plot(ps, means_arr, label=f'$n={n}$')
    ax.fill_between(ps, (means_arr-ci), (means_arr+ci), color='b', alpha=.1)
plt.title("$T(p)$")
plt.xlabel("$p$")
plt.savefig('T.pdf', bbox_inches='tight')
#plt.legend()
# %%
fig, ax = plt.subplots()
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci = 3* stds_arr
    lower = means_arr - ci
    upper = means_arr + ci
    ax.plot(ps, means_arr[0] / means_arr, label=f'$n={n}$')
    ax.fill_between(ps, (lower[0] / lower), (upper[0] / upper), color='b', alpha=.1)
plt.title("$S(p)$")
plt.xlabel("$p$")
plt.savefig('S.pdf', bbox_inches='tight')
#plt.legend()
# %%
fig, ax = plt.subplots()
ps_arr = np.array(ps)
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci =  3* stds_arr
    lower = means_arr - ci
    upper = means_arr + ci
    ax.plot(ps, means_arr[0] / means_arr / ps_arr, label=f'$n={n}$')
    ax.fill_between(ps, (lower[0] / lower / ps_arr), (upper[0] / upper / ps_arr), color='b', alpha=.1)
plt.title("$E(p)$")
plt.xlabel("$p$")
plt.savefig('E.pdf', bbox_inches='tight')
#plt.legend()
# %%
