#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "linalg.h"
#include "utils.h"
#pragma warning(disable:4996)

#define REPEATS 30
#define FILE_NAME_SIZE 35
#define SAVE_FINAL_TIME_LAYER 1
#define MAX_THREADS_POW 4
#define N_COLS 960
#define N_ROWS 1138
#define TIME_STEPS 100
#define LAMBDA 0.2
int main(int argc, char** argv) {
    int world_rank = 0, world_size = 1;
    char init_pic_name[FILE_NAME_SIZE] = "init_pic.npy";
    char result_pic_name[FILE_NAME_SIZE] = "res_pic.npy";
    int j;
    char data_name[FILE_NAME_SIZE] = { 0 };
    char txt[] = "_dim.txt";
    double start, end, time, time_squared;
    FILE* f = NULL;
    PMBuffer* buff;
    int k, p;
    itoa(N_COLS, data_name, 10);
    strcat(data_name, txt);
    f = fopen(data_name, "w");
    for (k = 0; k <= MAX_THREADS_POW; k++)
    {
        p = (int)pow(2, k);
        omp_set_num_threads(p);
        buff = malloc(sizeof(PMBuffer));
        if (world_rank == 0) {
            buff->name = calloc(FILE_NAME_SIZE, sizeof(char));
            buff->res_name = calloc(FILE_NAME_SIZE, sizeof(char));
            strcat(buff->name, init_pic_name);
            strcat(buff->res_name, result_pic_name);
        }
        buff->n_proc = world_size;
        buff->rank = world_rank;
        buff->n_cols = N_COLS;
        buff->n_rows = N_ROWS;
        buff->per_proc = buff->n_cols / buff->n_proc;
        buff->time_steps = TIME_STEPS;
        buff->lambda = LAMBDA;
        time = time_squared = 0;

        for (j = 0; j < REPEATS; j++) {
            pic_read(buff);
            start = omp_get_wtime();

            PMDiff(buff);

            end = omp_get_wtime();
            time += end - start;
            time_squared += (end - start) * (end - start);
            if ((j == REPEATS - 1) && SAVE_FINAL_TIME_LAYER)
                pic_write(buff);
            free(buff->I_cur);
            free(buff->I_prev);
        }
        time /= REPEATS;
        time_squared /= REPEATS;
        fprintf(f, "%i, %g, %g\n", p, time, sqrt(time_squared - time * time));
        free(buff->name);
        free(buff->res_name);
        free(buff);
    }
    fclose(f);
    return 0;
}