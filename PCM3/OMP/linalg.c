#include "linalg.h"
#include <omp.h>

double g(double x) {
    return exp(-x * x / K_PARAMETER / K_PARAMETER);
}
void apply_bc(PMBuffer* buff) {
    int i, j;
#pragma omp parallel for private(i)
    for (i = 1; i < buff->n_rows - 1; i++) {
        buff->I_cur[i] = (4 * buff->I_cur[i + buff->n_rows] - buff->I_cur[i + 2 * buff->n_rows]) / 3.;
    }

#pragma omp parallel for private(i)
    for (i = 1; i < buff->n_rows - 1; i++) {
        buff->I_cur[(buff->per_proc - 1) * buff->n_rows + i] = (4 * buff->I_cur[(buff->per_proc - 2) * buff->n_rows + i] - buff->I_cur[(buff->per_proc - 3) * buff->n_rows + i]) / 3.;
    }

#pragma omp parallel for private(j)
    for (j = 0; j < buff->per_proc; j++) {
        buff->I_cur[j * buff->n_rows] = (4 * buff->I_cur[j * buff->n_rows + 1] - buff->I_cur[j * buff->n_rows + 2]) / 3.;
        buff->I_cur[j * buff->n_rows + buff->n_rows - 1] = (4 * buff->I_cur[j * buff->n_rows + buff->n_rows - 2] - buff->I_cur[j * buff->n_rows + buff->n_rows - 3]) / 3.;
    }
}
void PMtime_step(PMBuffer* buff) {
    int i, j, k;
    double cN, cS, cW, cE;
    double dN, dS, dW, dE;
#pragma omp parallel for private(k, dN, dS, dE, dW, cN, cS, cE, cW)
    for (k = 1 + buff->n_rows; k <= (buff->n_rows) * (buff->per_proc - 2) + buff->n_rows - 2; k++) {
        dN = buff->I_prev[k - 1] - buff->I_prev[k];
        dS = buff->I_prev[k + 1] - buff->I_prev[k];
        dE = buff->I_prev[k + buff->n_rows] - buff->I_prev[k];
        dW = buff->I_prev[k - buff->n_rows] - buff->I_prev[k];
        cN = g(dN);
        cS = g(dS);
        cE = g(dE);
        cW = g(dW);
        buff->I_cur[k] = buff->I_prev[k] + buff->lambda * (cN * dN + cS * dS * cW * dW + cE * dE);
    }
}

void PMDiff(PMBuffer* buff) {
    int step;
    double* tmp;
    for (step = 0; step < buff->time_steps; step++) {
        tmp = buff->I_prev;
        buff->I_prev = buff->I_cur;
        buff->I_cur = tmp;
        PMtime_step(buff);
        apply_bc(buff);
    }
}



void Copy(double* dest, double* src, int n) {
    int i;
#pragma omp parallel for private(i) 
    for (i = 0; i < n; i++)
        dest[i] = src[i];
}
