#pragma once
typedef double (*real2Dfunc)(double x, double y);
#define K_PARAMETER 0.4
typedef struct PMBuffer {
    int rank, n_cols, n_rows, n_proc, per_proc;
    char* name, * res_name;
    double* fict_elements;
    int* scs, * sds, * rcs, * rds;

    double lambda;
    double* I_cur, * I_prev;
    int time_steps;

} PMBuffer;

void apply_bc(PMBuffer* buff);
void PMtime_step(PMBuffer* buff);
void PMDiff(PMBuffer* buff);
void Copy(double* dest, double* src, int n);
