#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#pragma warning(disable:4996)
void pic_read(PMBuffer* buff) {
    FILE* f;
    int i, j;
    double* row;
    f = fopen(buff->name, "rb");
    row = malloc(sizeof(double) * buff->n_cols);
    buff->I_cur = malloc(sizeof(double) * buff->n_rows * buff->n_cols);
    for (i = 0; i < buff->n_rows; i++) {
        //fread(buff->A + i * buff->n_cols, sizeof(double), buff->n_cols, f);
        fread(row, sizeof(double), buff->n_cols, f);
        for (j = 0; j < buff->n_cols; j++)
            buff->I_cur[j * buff->n_rows + i] = row[j];
    }
    fclose(f);
    free(row);
    buff->I_prev = malloc(sizeof(double) * buff->per_proc * buff->n_rows);
    Copy(buff->I_prev, buff->I_cur, buff->per_proc * buff->n_rows);
}

void pic_write(PMBuffer* buff) {
    FILE* f;
    int i, j;
    double* row = malloc(sizeof(double) * buff->n_cols);
    f = fopen(buff->res_name, "wb");
    for (i = 0; i < buff->n_rows; i++) {
        for (j = 0; j < buff->n_cols; j++)
            row[j] = buff->I_cur[j * buff->n_rows + i];
        //fwrite(buff->A + i * buff->n_cols, sizeof(double), buff->n_cols, f);
        fwrite(row, sizeof(double), buff->n_cols, f);
    }
    fclose(f);
    free(row);
}
