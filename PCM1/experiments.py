#%%
import pandas as pd
import numpy as np
import seaborn as sns
sns.set_style('whitegrid')
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('svg','pdf')
import matplotlib.pyplot as plt
import os 
#%%
ns = [32]
stds = {}
means = {}
ps = []
atleast_one_read = False
for n in ns:
    means[n] = []
    stds[n] = []
    with open(f"{n}_dim.txt") as f:
        lines = f.readlines()
        for line in lines:
            line_splitted = line[:-1].split(',')
            p = int(line_splitted[0])
            mean = float(line_splitted[1])
            std = float(line_splitted[2])
            if not atleast_one_read:
                ps.append(p)
            means[n].append(mean)
            stds[n].append(std)
    atleast_one_read = True
#%%
fig, ax = plt.subplots()
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci =  3*stds_arr
    ax.plot(ps, means_arr, label=f'$n={n}$')
    ax.fill_between(ps, (means_arr-ci), (means_arr+ci), color='b', alpha=.1)
plt.title("$T(p)$")
plt.xlabel("$p$")
plt.savefig('T.pdf', bbox_inches='tight')
#plt.legend()
# %%
fig, ax = plt.subplots()
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci = 3* stds_arr
    lower = means_arr - ci
    upper = means_arr + ci
    ax.plot(ps, means_arr[0] / means_arr, label=f'$n={n}$')
    ax.fill_between(ps, (lower[0] / lower), (upper[0] / upper), color='b', alpha=.1)
plt.title("$S(p)$")
plt.xlabel("$p$")
plt.savefig('S.pdf', bbox_inches='tight')
#plt.legend()
#%%
fig, ax = plt.subplots()
ps_arr = np.array(ps)
for n in means.keys():
    means_arr = np.array(means[n])
    stds_arr = np.array(stds[n])
    ci =  3* stds_arr
    lower = means_arr - ci
    upper = means_arr + ci
    ax.plot(ps, means_arr[0] / means_arr / ps_arr, label=f'$n={n}$')
    ax.fill_between(ps, (lower[0] / lower / ps_arr), (upper[0] / upper / ps_arr), color='b', alpha=.1)
plt.title("$E(p)$")
plt.xlabel("$p$")
plt.savefig('E.pdf', bbox_inches='tight')
#plt.legend()
# %%
L = 1.
reference_N = 32
approx_sol = np.fromfile(f"{reference_N}_dim_approx_sol.npy").reshape((reference_N + 2, reference_N + 2))
real_sol =  np.fromfile(f"{reference_N}_dim_real_sol.npy").reshape((reference_N + 2, reference_N + 2))
strip = np.linspace(0, L, reference_N + 2)
xx, yy = np.meshgrid(strip,strip)
# %%
fig, ax = plt.subplots(1,2, figsize=(6,3))
ax[0].contourf(xx,yy,approx_sol, levels=30)
ax[0].set_title("approx")
ax[1].set_title("real")
ax[1].contourf(xx,yy,real_sol, levels=30)
plt.savefig('solutions_cmp.pdf', bbox_inches='tight')
#%%
fig, ax = plt.subplots(figsize=(3,3))
fig=plt.contourf(xx,yy,np.abs(real_sol-approx_sol), levels=40)
fig.set_cmap('plasma')
plt.colorbar()
fig.set_clim(0,approx_sol.max())
plt.savefig('solutions_diff.pdf', bbox_inches='tight')
# %%

