#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include "linalg.h"
#include <string.h>
#include <math.h>
#pragma warning (disable:4996)
#define MAX_THREADS_POW 6
#define M_PI 3.14159265358979323846 
#define REPEATS 40
#define N 512
#define EPSILON 1e-6
#define TOLERANCE 1e-13
#define FILE_NAME_SIZE 35
static double L = 1.;
static double uB = 0.;
double F(double x, double y) {
	return 2 * (x * x - x + y * y - y);
}

int main() {
	int k, i;
	double T, T_sq;
	char data_name[FILE_NAME_SIZE] = { 0 };
	char appr_sol_name[FILE_NAME_SIZE] = { 0 };
	char real_sol_name[FILE_NAME_SIZE] = { 0 };
	double omega = 2 / (1 + sin(M_PI / (1 + N)));
	int dim = (N + 2) * (N + 2);
	double start, end;
	double* real_solution;
	PoissonBuffer* buff = createBuffer(N, L, F, uB);
	FILE* f;
	int p;
	char txt[] = "_dim.txt";
	char anpy[] = "_dim_approx_sol.npy";
	char rnpy[] = "_dim_real_sol.npy";
	itoa(N, data_name, 10);
	strcat(data_name, txt);
	f = fopen(data_name, "w");
	for (k = 0; k <= MAX_THREADS_POW; k++) {
		T = T_sq = 0;
		p = (int)pow(2, k);
		omp_set_num_threads(p);
		for (i = 0; i < REPEATS; i++) {
			initWithVal(buff->u_prev, uB, dim);
			initWithVal(buff->u_cur, uB, dim);
			start = omp_get_wtime();

			PoissonFDMsolveSOR_v3(N, omega, EPSILON, buff);

			end = omp_get_wtime();
			T += end - start;
			T_sq += (end - start) * (end - start);
		}
		T /= REPEATS;
		T_sq /= REPEATS;
		fprintf(f, "%i, %g, %g\n", p, T, sqrt(T_sq - T * T));
	}
	fclose(f);
	itoa(N, appr_sol_name, 10);
	strcat(appr_sol_name, anpy);
	f = fopen(appr_sol_name, "wb");
	fwrite(buff->u_prev, sizeof(double), dim, f);
	fclose(f);
	itoa(N, real_sol_name, 10);
	strcat(real_sol_name, rnpy);
	f = fopen(real_sol_name, "wb");
	real_solution = real_zero_boundary_sol(N, L);
	fwrite(real_solution, sizeof(double), dim, f);
	fclose(f);
	free(real_solution);
	destroyBuffer(buff);
	return 0;
}
