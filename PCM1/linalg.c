#include "linalg.h"
#include <omp.h>
#include <stdlib.h>
#include <math.h>
double dist(double* v1, double* v2, int dim) {
	int i;
	double res = 0;
#pragma omp parallel for private(i) reduction(+:res)
	for (i = 0; i < dim; i++)
		res += (v1[i] - v2[i]) * (v1[i] - v2[i]);
	return sqrt(res);
}
PoissonBuffer* createBuffer(int n, double L, real2Dfunc f, double uB) {
	PoissonBuffer* buff = malloc(sizeof(PoissonBuffer));
	double h = L / (n + 1);
	int i, j, k;
	double diff;
	int dim = (n + 2) * (n + 2);
	double* u_prev = calloc(dim, sizeof(double));
	double* u_cur = calloc(dim, sizeof(double));
	int reds_n = n * n / 2 + n % 2;
	int blacks_n = n * n / 2;
	int* reds = malloc(sizeof(int) * reds_n);
	double* reds_right_side = malloc(sizeof(double) * reds_n);
	int* blacks = malloc(sizeof(int) * blacks_n);
	double* blacks_right_side = malloc(sizeof(double) * blacks_n);
	int red_cnt = 0;
	int black_cnt = 0;
	for (i = 1; i < n + 1; i++)
		for (j = 1; j < n + 1; j++) {
			if ((i + j) % 2) {
				k = i * (n + 2) + j;
				blacks[black_cnt] = k;
				blacks_right_side[black_cnt] = -h * h * f(i * h, j * h);
				black_cnt++;
			}
			else {
				k = i * (n + 2) + j;
				reds[red_cnt] = k;
				reds_right_side[red_cnt] = -h * h * f(i * h, j * h);
				red_cnt++;
			}

		}
	if (fabs(uB) > TOLERANCE) {
#pragma omp parallel for private(i, k)
		for (i = 0; i < n + 2; i++) {
			k = (n + 2) * i;
			u_prev[k] = u_cur[k] = uB;
			k += n + 1;
			u_prev[k] = u_cur[k] = uB;

			k = i;
			u_prev[k] = u_cur[k] = uB;
			k += (n + 1) * (n + 2);
			u_prev[k] = u_cur[k] = uB;
		}
	}
	buff->u_cur = u_cur;
	buff->u_prev = u_prev;
	buff->reds = reds;
	buff->blacks = blacks;
	buff->blacks_right_side = blacks_right_side;
	buff->reds_right_side = reds_right_side;
	return buff;
}
void destroyBuffer(PoissonBuffer* buff) {
	free(buff->u_cur);
	free(buff->u_prev);
	free(buff->reds);
	free(buff->blacks);
	free(buff->reds_right_side);
	free(buff->blacks_right_side);
	free(buff);
}

double* real_zero_boundary_sol(int n, double L) {
	double h = L / (n + 1);
	int i, j, k;
	double diff;
	int dim = (n + 2) * (n + 2);
	double x, y;
	double* u = calloc(dim, sizeof(double));
#pragma omp parallel for collapse(2) private(i, j, k, x, y)
	for (i = 1; i < n + 1; i++)
		for (j = 1; j < n + 1; j++) {
			k = i * (n + 2) + j;
			x = i * h;
			y = j * h;
			u[k] = x * (x - L) * y * (y - L);
		}
	return u;

}

void initWithVal(double* vec, double val, int dim) {
	int i;
#pragma omp parallel for private(i)
	for (i = 0; i < dim; i++)
		vec[i] = val;
}

void PoissonFDMsolveSOR_v3(int n, double omega, double eps, PoissonBuffer* buff) {
	int i, j, k;
	double diff;
	int dim = (n + 2) * (n + 2);
	double* u_prev = buff->u_prev;
	double* u_cur = buff->u_cur;
	int reds_n = n * n / 2 + n % 2;
	int blacks_n = n * n / 2;
	int* reds = buff->reds;
	double* reds_right_side = buff->reds_right_side;
	int* blacks = buff->blacks;
	double* blacks_right_side = buff->blacks_right_side;
	double* tmp;
	buff->iters_num = 0;
	do {
#pragma omp parallel for private(i, k)
		for (i = 0; i < reds_n; i++) {
			k = reds[i];
			u_cur[k] = 0.25 * omega * (u_prev[k + n + 2] + u_prev[k - n - 2] + u_prev[k - 1] + u_prev[k + 1] + reds_right_side[i]) + (1. - omega) * u_prev[k];
		}
#pragma omp parallel for private(i, k)
		for (i = 0; i < blacks_n; i++) {
			k = blacks[i];
			u_cur[k] = 0.25 * omega * (u_cur[k + n + 2] + u_cur[k - n - 2] + u_cur[k - 1] + u_cur[k + 1] + blacks_right_side[i]) + (1. - omega) * u_prev[k];
		}
		diff = dist(u_cur, u_prev, dim);
		tmp = u_prev;
		u_prev = u_cur;
		u_cur = tmp;
		buff->iters_num++;
	} while (diff > eps);
}