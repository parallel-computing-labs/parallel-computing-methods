#pragma once
#define TOLERANCE 1e-13
typedef double (*real2Dfunc)(double x, double y);
typedef struct PoissonBuffer {
	double* u_prev;
	double* u_cur;
	int* reds;
	double* reds_right_side;
	int* blacks;
	double* blacks_right_side;
	int iters_num;

} PoissonBuffer;
PoissonBuffer* createBuffer(int n, double L, real2Dfunc f, double uB);
void destroyBuffer(PoissonBuffer* buff);
double dist(double* v1, double* v2, int dim);
double* real_zero_boundary_sol(int n, double L);
void initWithVal(double* vec, double val, int dim);
void PoissonFDMsolveSOR_v3(int n, double omega, double eps, PoissonBuffer* buff);